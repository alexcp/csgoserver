FROM ubuntu:14.04

MAINTAINER Alex Croteau-Pothier <alex@alexcp.com>

ENV DEBIAN_FRONTEND noninteractive

COPY etc/apt/sources.list /etc/apt/sources.list

# Install dependencies
RUN apt-get update &&\
    apt-get install -y tmux mailutils postfix curl lib32gcc1 libstdc++6 wget

# Create csgoserver user
RUN useradd -ms /bin/bash csgoserver

USER csgoserver
WORKDIR /home/csgoserver

RUN cd /home/csgoserver &&\
  wget http://gameservermanagers.com/dl/csgoserver &&\
  chmod +x csgoserver

RUN cd /home/csgoserver &&\
    yes | ./csgoserver install